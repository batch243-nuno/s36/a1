const Task = require('../models/task');

// controller function to get a task
module.exports.getTask = (taskId) => {
  return Task.findById(taskId).then((result) => {
    return result;
  });
};

// controller function for creating a task
module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name,
  });
  return newTask.save().then((task, error) => {
    return error ? false : task;
  });
};

// controller function for updating status
module.exports.updateStatus = (taskId, requestBody) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      result.status = requestBody.status;
      return result.save().then((updatedStatus, saveError) => {
        return saveError ? false : updatedStatus;
      });
    }
  });
};
