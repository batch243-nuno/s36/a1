const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');

module.exports = router;

// GET a task
router.get('/:id', (request, response) => {
  taskController
    .getTask(request.params.id)
    .then((resultFromController) => response.send(resultFromController));
});

// POST create a new task
router.post('/', (request, response) => {
  taskController
    .createTask(request.body)
    .then((resultFromController) =>
      response.send(`${resultFromController.name} was created.`)
    );
});

// PUT update status
router.put('/:id', (request, response) => {
  taskController
    .updateStatus(request.params.id, request.body)
    .then((resultFromController) =>
      response.send(
        `${resultFromController.name}'s status updated to ${resultFromController.status}`
      )
    );
});
