const express = require('express');
const app = express();

const mongoose = require('mongoose');
const port = 3001;

const taskRoute = require('./routes/taskRoute');
mongoose.connect(
  'mongodb+srv://admin:admin@zuittbatch243.1muzoob.mongodb.net/s36-Activity?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("We're connected to cloud database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(port, () => console.log(`Server is running in localhost:${port}`));
app.use('/tasks', taskRoute);
