const mongoose = require('mongoose');

// mongooseSchema
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: 'Pending',
  },
});
// model(<database name>, <schema>)
module.exports = mongoose.model('Task', taskSchema);
